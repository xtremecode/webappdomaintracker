const express = require('express');
const app = express();

// Create link to Angular build directory
app.use(express.static(__dirname + '/dist'));
app.listen(process.env.PORT || 8080);