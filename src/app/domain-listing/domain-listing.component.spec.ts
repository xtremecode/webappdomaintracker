import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DomainListingComponent } from './domain-listing.component';

describe('DomainListingComponent', () => {
  let component: DomainListingComponent;
  let fixture: ComponentFixture<DomainListingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DomainListingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DomainListingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
