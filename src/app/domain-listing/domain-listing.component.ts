import { Component, OnInit } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';

@Component({
  selector: 'app-domain-listing',
  templateUrl: './domain-listing.component.html',
  styleUrls: ['./domain-listing.component.css']
})
export class DomainListingComponent implements OnInit {

  //domainmostvisited: Array<any> = domainmostvisited;
  domainmostvisited: Array<any>;
  error: string;
  constructor(private http: Http) { }

  ngOnInit() {
    //To go for local json
    //this.http.get('data/domainlist.json')

    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    let options = new RequestOptions({ headers: headers });
    
    this.http.get('https://wsdomaintracker.herokuapp.com/tracker/treeMostVisited', options )
      .map(res => res.json())
      .subscribe(
        data => this.domainmostvisited = data,
        error => this.error = error.statusText
      );
    
  }

}
