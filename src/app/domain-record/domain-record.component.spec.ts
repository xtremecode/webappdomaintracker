import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DomainRecordComponent } from './domain-record.component';

describe('DomainRecordComponent', () => {
  let component: DomainRecordComponent;
  let fixture: ComponentFixture<DomainRecordComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DomainRecordComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DomainRecordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
