import { Component, OnInit, Input} from '@angular/core';
import { Domain } from './../domain';

@Component({
  selector: 'app-domain-record',
  templateUrl: './domain-record.component.html',
  styleUrls: ['./domain-record.component.css']
})
export class DomainRecordComponent implements OnInit {

  @Input('domain') domain: Domain;
  
  constructor() { }

  ngOnInit() {
  }

}
